﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using clinical.Models;
using Microsoft.AspNetCore.Mvc;
using clinical.Data;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace clinical.ViewComponents
{
    public class UserMenuViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _context;
        private string _userId;

        public UserMenuViewComponent(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _userId = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            
        }

        public IViewComponentResult Invoke(string ViewName)
        {
            var model = _context.Users.FirstOrDefault(u => u.Id == _userId);
            var view = ViewName ?? "Default";
            return View(view, model);
        }

    }
}
