﻿// Write your Javascript code.
$(document).ready(function () {
    $("#pacientes-table").DataTable({
        language: {
            url: 'https://cdn.datatables.net/plug-ins/1.10.12/i18n/Spanish.json'
        }
    });
    $("#doctores-table").DataTable({
        language: {
            url: 'https://cdn.datatables.net/plug-ins/1.10.12/i18n/Spanish.json'
        }
    });
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        language: 'es',
        startView: 'decade'
    });
    
    $("#AgregarAtencion").click(function () {
        $.get("/Atenciones/_Create?pacienteId=" + $("#PacienteId").val(), function (data) {
            $("#crearAtencionModalContent").html(data);
            $(".textarea").wysihtml5({ locale: "es-AR", size: "sm", link: false, image: false });
        });
    });

    $(".editAtencion").click(function () {
        var id = $(this).data("id");
        $.get("/Atenciones/_Edit/" + id, function (data) {
            $("#editarAtencionModalContent").html(data);
            $(".textarea").wysihtml5({ locale: "es-AR", size: "sm", link: false, image: false });
        });
    });
    if (jQuery("#pacienteAutocomplete").length) {
        $("#pacienteAutocomplete").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/Pacientes/autoComplete',
                    type: 'POST',
                    cache: false,
                    data: request,
                    dataType: 'json',
                    success: function (data) {
                        response($.map(data, function (item) {
                            return item;
                        }))
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            select: function (event, ui) {
                var elem = $(event.originalEvent.toElement);
                if (elem.hasClass('ac-item-a')) {
                    var url = elem.attr('data-url');
                    event.preventDefault();
                    window.open(url, '_blank ');
                }
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            return $("<li></li>")
                .data("item.autocomplete", item)
                .append('<a href="/Pacientes/Details/' + item.val +'" target="_blank">' + item.label + '<span class="ac-item-a" data-url="/Pacientes/Details/' + item.val + '">(Ir al Paciente)</span></a>')
                .appendTo(ul);
        };
    }

    if (jQuery("#doctoresAutocomplete").length) {
        $("#doctoresAutocomplete").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/Doctores/autoComplete',
                    type: 'POST',
                    cache: false,
                    data: request,
                    dataType: 'json',
                    success: function (data) {
                        response($.map(data, function (item) {
                            return item;
                        }))
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            select: function (event, ui) {
                var elem = $(event.originalEvent.toElement);
                if (elem.hasClass('ac-item-a')) {
                    var url = elem.attr('data-url');
                    event.preventDefault();
                    window.open(url, '_blank ');
                }
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            console.log(item)
            return $("<li></li>")
                .data("item.autocomplete", item)
                .append('<a>' + item.label + '<span class="ac-item-a" data-url="/Doctores/Details/' + item.val + '">(Ir al Doctor)</span></a>')
                .appendTo(ul);
        };
    }
});
