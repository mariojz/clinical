﻿using clinical.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace clinical.Helpers
{
    public class PacientesHelper
    {
        private readonly ApplicationDbContext _context;

        public PacientesHelper(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<SelectListItem> SelectPrevision(string nombre = null)
        {
            var previsiones = new List<SelectListItem>();
            foreach (var prevision in _context.Prevision.OrderBy(d => d.Nombre))
            {
                if (prevision.Nombre == nombre)
                {
                    previsiones.Add(new SelectListItem
                    {
                        Value = prevision.Nombre,
                        Text = prevision.Nombre,
                        Selected = true
                    });
                }
                else
                {
                    previsiones.Add(new SelectListItem
                    {
                        Value = prevision.Nombre,
                        Text = prevision.Nombre
                    });
                }
            }
            return previsiones;
        }

        public List<SelectListItem> SelectEstadoCivil(string nombre = null)
        {
            var estados = new List<SelectListItem>();

            estados.Add(new SelectListItem
            {
                Value = "Casado",
                Text = "Casado",
                Selected = nombre == "Casado"
            });
            estados.Add(new SelectListItem
            {
                Value = "Separado",
                Text = "Separado",
                Selected = nombre == "Separado"
            });
            estados.Add(
                new SelectListItem
                {
                    Value = "Soltero",
                    Text = "Soltero",
                    Selected = nombre == "Soltero"
                });
            estados.Add(new SelectListItem
            {
                Value = "Viudo",
                Text = "Viudo",
                Selected = nombre == "Viudo"
            });

            return estados;
        }
    }
}
