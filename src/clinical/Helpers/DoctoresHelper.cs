﻿using clinical.Data;
using clinical.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace clinical.Helpers
{
    public class DoctoresHelper
    {
        private readonly ApplicationDbContext _context;
        private string _idEmpresa;

        public DoctoresHelper(ApplicationDbContext context, string idEmpresa)
        {
            _context = context;
            _idEmpresa = idEmpresa;
        }

        public List<SelectListItem> SelectDoctores(int? id = null)
        {
            var doctores = new List<SelectListItem>();
            foreach (var doctor in _context.Doctor.Where(d => d.IdEmpresa.Equals(_idEmpresa)).OrderBy(d => d.Nombre))
            {
                if (id != null && id == doctor.Id)
                {
                    doctores.Add(new SelectListItem
                    {
                        Value = doctor.Id.ToString(),
                        Text = doctor.Nombre + " " + doctor.ApPaterno,
                        Selected = true
                    });
                }
                else
                {
                    doctores.Add(new SelectListItem
                    {
                        Value = doctor.Id.ToString(),
                        Text = doctor.Nombre + " " + doctor.ApPaterno
                    });
                }
            }
            return doctores;
        }

        public List<SelectListItem> SelectEspecialidad(string nombre = null)
        {
            var especialidades = new List<SelectListItem>();
            foreach (var especialidad in _context.Especialidad.OrderBy(d => d.Nombre))
            {
                if (especialidad.Nombre == nombre)
                {
                    especialidades.Add(new SelectListItem
                    {
                        Value = especialidad.Nombre,
                        Text = especialidad.Nombre,
                        Selected = true
                    });
                }
                else
                {
                    especialidades.Add(new SelectListItem
                    {
                        Value = especialidad.Nombre,
                        Text = especialidad.Nombre
                    });
                }
            }
            return especialidades;
        }

    }
}
