﻿using Microsoft.AspNetCore.Http;
using System.IO;

namespace clinical.Helpers
{
    public static class FileHelper
    {
        public static byte[] ConvertToBytes(IFormFile file)
        {
            byte[] fileAsByte = null;
            var reader = new BinaryReader(file.OpenReadStream());
            fileAsByte = reader.ReadBytes((int)file.Length);
            return fileAsByte;
        }
    }
}
