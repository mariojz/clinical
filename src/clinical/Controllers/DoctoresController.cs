using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using clinical.Data;
using clinical.Helpers;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace clinical.Models
{
    public class DoctoresController : Controller
    {
        private readonly ApplicationDbContext _context;
        private ApplicationUser _user;

        public DoctoresController(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            var userId = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            _user = _context.Users.FirstOrDefault(u => u.Id == userId);
        }

        // GET: Doctores
        public async Task<IActionResult> Index()
        {
            return View(await _context.Doctor.Where(d => d.IdEmpresa.Equals(_user.IdEmpresa)).ToListAsync());
        }

        // GET: Doctores/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var doctor = await _context.Doctor.Where(d => d.IdEmpresa.Equals(_user.IdEmpresa)).Include(d => d.Pacientes).Include(d => d.Atenciones).ThenInclude(a => a.Paciente).SingleOrDefaultAsync(m => m.Id == id);
            if (doctor == null)
            {
                return NotFound();
            }

            return View(doctor);
        }

        // GET: Doctores/Create
        public IActionResult Create()
        {
            ViewBag.Especialidades = new DoctoresHelper(_context, _user.IdEmpresa).SelectEspecialidad();
            return View();
        }

        // POST: Doctores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ApMaterno,ApPaterno,Email,Especialidad,Nombre,Rut,Telefono,FecNacimiento,Sexo")] Doctor doctor)
        {
            if (ModelState.IsValid)
            {
                doctor.IdEmpresa = _user.IdEmpresa;
                _context.Add(doctor);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", new { id = doctor.Id });
            }
            return View(doctor);
        }

        // GET: Doctores/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var doctor = await _context.Doctor.SingleOrDefaultAsync(m => m.Id == id);
            if (doctor == null)
            {
                return NotFound();
            }
            ViewBag.Especialidades = new DoctoresHelper(_context, _user.IdEmpresa).SelectEspecialidad(doctor.Especialidad);
            return View(doctor);
        }

        // POST: Doctores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ApMaterno,ApPaterno,Email,Especialidad,Nombre,Rut,Telefono,FecNacimiento,Sexo")] Doctor doctor)
        {
            if (id != doctor.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    doctor.IdEmpresa = _user.IdEmpresa;
                    _context.Update(doctor);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DoctorExists(doctor.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", new { id = doctor.Id });
            }
            return View(doctor);
        }

        // GET: Doctores/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var doctor = await _context.Doctor.SingleOrDefaultAsync(m => m.Id == id && m.IdEmpresa.Equals(_user.IdEmpresa));
            if (doctor == null)
            {
                return NotFound();
            }

            return View(doctor);
        }

        // POST: Doctores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var doctor = await _context.Doctor.SingleOrDefaultAsync(m => m.Id == id && m.IdEmpresa.Equals(_user.IdEmpresa));
            _context.Doctor.Remove(doctor);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool DoctorExists(int id)
        {
            return _context.Doctor.Any(e => e.Id == id && e.IdEmpresa.Equals(_user.IdEmpresa));
        }
    }
}
