using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using clinical.Data;
using clinical.Models;
using clinical.Helpers;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IO;

namespace clinical.Controllers
{
    public class PacientesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private ApplicationUser _user;

        public PacientesController(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            var userId = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            _user = _context.Users.FirstOrDefault(u => u.Id == userId);
        }

        // GET: Pacientes
        public async Task<IActionResult> Index()
        {
            ViewData["BreadCrumbLevel1"] = "Pacientes";
            ViewData["BreadCrumbLevel1Link"] = "/Pacientes";
            return View(await _context.Paciente.Where(p => p.IdEmpresa.Equals(_user.IdEmpresa)).Include(paciente => paciente.Doctor).ToListAsync());
        }

        // GET: Pacientes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            ViewData["BreadCrumbLevel1"] = "Pacientes";
            ViewData["BreadCrumbLevel1Link"] = "/Pacientes";
            if (id == null)
            {
                return NotFound();
            }

            var paciente = await _context.Paciente.SingleOrDefaultAsync(m => m.Id == id && m.IdEmpresa.Equals(_user.IdEmpresa));
            if (paciente == null)
            {
                return NotFound();
            }

            paciente.Adjuntos = await _context.Adjunto.Where(a => a.PacienteId == id).Select(a => new Adjunto{ Id = a.Id, Nombre = a.Nombre}).ToListAsync();
            paciente.AdjuntosHistoricos = await _context.AdjuntoHistorico.Where(a => a.PacienteId == id).Select(a => new AdjuntoHistorico
            {
                Id = a.Id,
                Nombre = a.Nombre
            }).ToListAsync();
            paciente.Atenciones = await _context.Atencion.Where(a => a.PacienteId == id).Include(a => a.Doctor).OrderByDescending(a => a.TimeStamp).ThenBy(a => a.Id).ToListAsync();

            return View(paciente);
        }

        // GET: Pacientes/Create
        public IActionResult Create()
        {
            ViewBag.Doctores = new DoctoresHelper(_context, _user.IdEmpresa).SelectDoctores();
            var pacienteHelper = new PacientesHelper(_context);
            ViewBag.Previsiones = pacienteHelper.SelectPrevision();
            ViewBag.Estados = pacienteHelper.SelectEstadoCivil();
            return View();
        }

        // POST: Pacientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ApMaterno,ApPaterno,Direccion,DoctorId,Edad,EstadoCivil,FecNacimiento,IdEmpresa,Nombre,Prevision,Rut,Sexo,Email,Telefono,Ocupacion")] Paciente paciente)
        {
            if (PacienteExists(paciente.Rut))
            {
                ModelState.AddModelError("Rut","El Paciente " + paciente.Rut + " ya se encuentra registrado.");
            }
            if (ModelState.IsValid)
            {
                paciente.IdEmpresa = _user.IdEmpresa;
                _context.Add(paciente);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", new { id = paciente.Id });
            }
            return View(paciente);
        }

        // GET: Pacientes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var paciente = await _context.Paciente.SingleOrDefaultAsync(m => m.Id == id);
            if (paciente == null)
            {
                return NotFound();
            }
            
            ViewBag.Doctores = new DoctoresHelper(_context, _user.IdEmpresa).SelectDoctores(paciente.DoctorId);
            var pacienteHelper = new PacientesHelper(_context);
            ViewBag.Previsiones = pacienteHelper.SelectPrevision(paciente.Prevision);
            ViewBag.Estados = pacienteHelper.SelectEstadoCivil(paciente.EstadoCivil);
            return View(paciente);
        }

        // POST: Pacientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ApMaterno,ApPaterno,Direccion,DoctorId,Edad,EstadoCivil,FecNacimiento,IdEmpresa,Nombre,Prevision,Rut,Sexo,Email,Telefono,Ocupacion")] Paciente paciente)
        {
            if (id != paciente.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    paciente.IdEmpresa = _user.IdEmpresa;
                    _context.Update(paciente);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PacienteExists(paciente.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", new { id = paciente.Id });
            }
            return View(paciente);
        }

        // GET: Pacientes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var paciente = await _context.Paciente.SingleOrDefaultAsync(m => m.Id == id && m.IdEmpresa.Equals(_user.IdEmpresa));
            if (paciente == null)
            {
                return NotFound();
            }

            return View(paciente);
        }

        // POST: Pacientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var paciente = await _context.Paciente.SingleOrDefaultAsync(m => m.Id == id && m.IdEmpresa.Equals(_user.IdEmpresa));
            _context.Paciente.Remove(paciente);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PacienteExists(int id)
        {
            return _context.Paciente.Any(e => e.Id == id && e.IdEmpresa.Equals(_user.IdEmpresa));
        }

        private bool PacienteExists(string rut)
        {
            return _context.Paciente.Any(e => e.Rut == rut && e.IdEmpresa.Equals(_user.IdEmpresa));
        }

        [HttpPost]
        public async Task<IActionResult> Upload(int id)
        {
            var files = Request.Form.Files;
            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    // TODO: Do domething with the files
                    var adjunto = new Adjunto {
                        Nombre = file.FileName,
                        ContentType = file.ContentType,
                        Archivo = FileHelper.ConvertToBytes(file),
                        PacienteId = id
                    };

                    _context.Add(adjunto);
                }

            }
            await _context.SaveChangesAsync();
            return Json(new { Status = "Ok" });
        }

        [HttpPost]
        public async Task<IActionResult> UploadHistorical(int id)
        {
            var files = Request.Form.Files;
            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    // TODO: Do domething with the files
                    var adjunto = new AdjuntoHistorico
                    {
                        Nombre = file.FileName,
                        ContentType = file.ContentType,
                        Archivo = FileHelper.ConvertToBytes(file),
                        PacienteId = id
                    };

                    _context.Add(adjunto);
                }

            }
            await _context.SaveChangesAsync();
            return Json(new { Status = "Ok" });
        }

    }
}
