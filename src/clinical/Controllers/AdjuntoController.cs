﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using clinical.Models.HomeViewModels;
using clinical.Data;
using clinical.Models;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.IO;

namespace clinical.Controllers
{
    public class AdjuntoController : Controller
    {
        private readonly ApplicationDbContext _context;
        private ApplicationUser _user;

        public AdjuntoController(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            var userId = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            _user = _context.Users.FirstOrDefault(u => u.Id == userId);
        }

        public async Task<IActionResult> Download(int id)
        {
            var file = await _context.Adjunto.Include(a => a.Paciente).SingleOrDefaultAsync(a => a.Id == id && a.Paciente.IdEmpresa.Equals(_user.IdEmpresa));
            if (file != null)
            {
                var stream = new MemoryStream();
                stream.Write(file.Archivo, 0, file.Archivo.Length);
                stream.Position = 0;
                return new FileStreamResult(stream, file.ContentType);
            }
            return NotFound("Archivo no encontrado");
        }

        public async Task<IActionResult> DownloadHistorical(int id)
        {
            var file = await _context.AdjuntoHistorico.Include(a => a.Paciente).SingleOrDefaultAsync(a => a.Id == id && a.Paciente.IdEmpresa.Equals(_user.IdEmpresa));
            if (file != null)
            {
                var stream = new MemoryStream();
                stream.Write(file.Archivo, 0, file.Archivo.Length);
                stream.Position = 0;
                return new FileStreamResult(stream, file.ContentType);
            }
            return NotFound("Archivo no encontrado");
        }
    }
}
