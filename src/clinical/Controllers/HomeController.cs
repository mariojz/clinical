﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using clinical.Models.HomeViewModels;
using clinical.Data;
using clinical.Models;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace clinical.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private ApplicationUser _user;

        public HomeController(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            var userId = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            _user = _context.Users.FirstOrDefault(u => u.Id == userId);
        }

        public IActionResult Index()
        {
            var model = new HomeViewModel()
            {
                DoctoresCount = _context.Doctor.Where(d => d.IdEmpresa.Equals(_user.IdEmpresa)).Count(),
                PacientesCount = _context.Paciente.Where(p => p.IdEmpresa.Equals(_user.IdEmpresa)).Count()
            };
            
            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
