using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using clinical.Data;
using clinical.Models;
using clinical.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace clinical.Controllers
{
    [Authorize]
    public class AtencionesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private ApplicationUser _user;

        public AtencionesController(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            var userId = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            _user = _context.Users.FirstOrDefault(u => u.Id == userId);
        }

        // GET: Atenciones
        public async Task<IActionResult> Index()
        {
            return View(await _context.Atencion.ToListAsync());
        }

        // GET: Atenciones/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var atencion = await _context.Atencion.SingleOrDefaultAsync(m => m.Id == id);
            if (atencion == null)
            {
                return NotFound();
            }

            return View(atencion);
        }

        // GET: Atenciones/Create
        public IActionResult _Create(int pacienteId)
        {
            var paciente = _context.Paciente.SingleOrDefault(m => m.Id == pacienteId);
            if (paciente == null)
            {
                return NotFound();
            }
            ViewBag.PacienteId = pacienteId;
            ViewBag.Doctores = new DoctoresHelper(_context, _user.IdEmpresa).SelectDoctores(paciente.DoctorId);
            ViewBag.Paciente = paciente;
            return View();
        }

        // GET: Atenciones/Create
        public IActionResult Create(int pacienteId)
        {
            ViewBag.PacienteId = pacienteId;
            return View();
        }

        // POST: Atenciones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Comentario,DoctorId,Fecha,PacienteId,TimeStamp,Tipo")] Atencion atencion)
        {
            if (ModelState.IsValid)
            {
                atencion.TimeStamp = DateTime.Now;
                _context.Add(atencion);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "Pacientes", new { id = atencion.PacienteId });
            }
            return RedirectToAction("Details", "Pacientes", new { id = atencion.PacienteId });
        }

        // GET: Atenciones/Create
        public async Task<IActionResult> _Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var atencion = await _context.Atencion.Include(a => a.Paciente).SingleOrDefaultAsync(m => m.Id == id);
            if (atencion == null)
            {
                return NotFound();
            }
            ViewBag.PacienteId = atencion.PacienteId;
            ViewBag.Doctores = new DoctoresHelper(_context, _user.IdEmpresa).SelectDoctores(atencion.DoctorId);
            ViewBag.Paciente = atencion.Paciente;
            return View(atencion);
        }

        // GET: Atenciones/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var atencion = await _context.Atencion.SingleOrDefaultAsync(m => m.Id == id);
            if (atencion == null)
            {
                return NotFound();
            }
            return View(atencion);
        }

        // POST: Atenciones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Comentario,DoctorId,Fecha,PacienteId,TimeStamp,Tipo")] Atencion atencion)
        {
            if (id != atencion.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(atencion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AtencionExists(atencion.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", "Pacientes", new { id = atencion.PacienteId });
            }
            return RedirectToAction("Details", "Pacientes", new { id = atencion.PacienteId });
        }

        // GET: Atenciones/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var atencion = await _context.Atencion.SingleOrDefaultAsync(m => m.Id == id);
            if (atencion == null)
            {
                return NotFound();
            }

            return View(atencion);
        }

        // POST: Atenciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var atencion = await _context.Atencion.SingleOrDefaultAsync(m => m.Id == id);
            _context.Atencion.Remove(atencion);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool AtencionExists(int id)
        {
            return _context.Atencion.Any(e => e.Id == id);
        }
    }
}
