﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace clinical.Models
{
    public class Especialidad
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
