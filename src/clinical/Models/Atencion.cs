﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace clinical.Models
{
    public class Atencion
    {
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Fecha { get; set; }
        public string Tipo { get; set; }
        [DataType(DataType.MultilineText)]
        public string Comentario { get; set; }

        [ForeignKey("PacienteId")]
        public int PacienteId { get; set; }
        public virtual Paciente Paciente { get; set; }

        [Display(Name = "Profesional")]
        [ForeignKey("DoctorId")]
        public int DoctorId { get; set; }
        public virtual Doctor Doctor { get; set; }
    }
}
