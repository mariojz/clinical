﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace clinical.Models
{
    public class Doctor
    {
        [Key]
        public int Id { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string IdEmpresa { get; set; }

        [Required]
        [RegularExpression(@"\d{3,8}-[\d|kK]{1}",ErrorMessage = "Debes ingresar un Rut valido. Ej: 12345678-9.")]
        public string Rut { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        [Display(Name = "Apellido Paterno")]
        public string ApPaterno { get; set; }
        [Display(Name = "Apellido Materno")]
        public string ApMaterno { get; set; }

        [Display(Name = "Fecha de Nacimiento")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime FecNacimiento { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [DataType(DataType.PhoneNumber)]
        public string Telefono { get; set; }

        public string Sexo { get; set; }
        [Display(Name = "Estado Civil")]
        public string EstadoCivil { get; set; }
        [Display(Name = "Dirección")]
        public string Direccion { get; set; }

        public string Especialidad { get; set; }

        public virtual ICollection<Paciente> Pacientes { get; set; }
        public virtual ICollection<Atencion> Atenciones { get; set; }

        [Display(Name = "Nombre Completo")]
        public string NombreCompleto
        {
            get
            {
                var name = Nombre + " " + ApPaterno;
                if (string.IsNullOrWhiteSpace(ApMaterno)) { name += " " + ApMaterno; }
                return name;
            }
        }

        public int Edad
        {
            get
            {
                DateTime now = DateTime.Today;
                int age = now.Year - FecNacimiento.Year;
                if (now < FecNacimiento.AddYears(age)) age--;
                return age;
            }
        }
    }
}
