﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace clinical.Models.HomeViewModels
{
    public class HomeViewModel
    {
        public int DoctoresCount { get; set; }
        public int PacientesCount { get; set; }
    }
}
