﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using clinical.Data;
using System;
using System.Linq;

namespace clinical.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ApplicationDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
            {
                // Look for prevision.
                if (!context.Prevision.Any())
                {
                    context.Prevision.AddRange(
                         new Prevision { Nombre = "Banmédica" },
                         new Prevision { Nombre = "Chuquicamata" },
                         new Prevision { Nombre = "Colmena Golden Cross" },
                         new Prevision { Nombre = "Consalud" },
                         new Prevision { Nombre = "Cruz Blanca" },
                         new Prevision { Nombre = "Cruz del Norte" },
                         new Prevision { Nombre = "FONASA" },
                         new Prevision { Nombre = "Fundación" },
                         new Prevision { Nombre = "Fusat" },
                         new Prevision { Nombre = "Masvida" },
                         new Prevision { Nombre = "Optima" },
                         new Prevision { Nombre = "Río Blanco" },
                         new Prevision { Nombre = "San Lorenz" },
                         new Prevision { Nombre = "Vida Tres" }
                    );
                    context.SaveChanges();
                }

                // Look for especialidades.
                if (!context.Especialidad.Any())
                {
                    context.Especialidad.AddRange(
                         new Especialidad { Nombre = "Dermatología" },
                         new Especialidad { Nombre = "Endocrinología" },
                         new Especialidad { Nombre = "Fonoaudiología" },
                         new Especialidad { Nombre = "Kinesiología" },
                         new Especialidad { Nombre = "Neurología" }
                    );
                    context.SaveChanges();
                }

                if (!context.Doctor.Any())
                {
                    context.Doctor.AddRange(
                        new Doctor {
                                  Rut = "62341251-3",
                                  Nombre = "Carlos",
                                  ApPaterno = "Hudson",
                                  ApMaterno = "Wood",
                                  Email = "cwood0@dagondesign.com",
                                  Sexo = "M",
                                  FecNacimiento = new DateTime(1981,1,14),
                                  Telefono = "46-(329)723-5891"
                                }, 
                        new Doctor {
                                  Rut = "63754098-5",
                                  Nombre = "Ryan",
                                  ApPaterno = "Boyd",
                                  ApMaterno = "Richardson",
                                  Email = "rrichardson1@disqus.com",
                                  Sexo = "M",
                                  FecNacimiento = new DateTime(1990, 6, 14),
                                  Telefono = "1-(812)367-0393"
                                }, 
                        new Doctor {
                                  Rut = "53509243-7",
                                  Nombre = "Clarence",
                                  ApPaterno = "Daniels",
                                  ApMaterno = "Nguyen",
                                  Email = "cnguyen2@clickbank.net",
                                  Sexo = "M",
                                  FecNacimiento = new DateTime(1985, 11, 14),
                                  Telefono = "81-(581)238-9560"
                                }
                        );
                    context.SaveChanges();
                }
            }
        }
    }
}