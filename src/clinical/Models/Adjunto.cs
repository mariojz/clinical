﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace clinical.Models
{
    public class Adjunto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string ContentType { get; set; }
        [Display(Name = "Adjunto")]
        public byte[] Archivo { get; set; }

        [ForeignKey("PacienteId")]
        public int PacienteId { get; set; }
        public virtual Paciente Paciente { get; set; }
    }
}
