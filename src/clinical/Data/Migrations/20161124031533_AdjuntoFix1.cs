﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace clinical.Data.Migrations
{
    public partial class AdjuntoFix1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Adjunto",
                table: "Atencion");

            migrationBuilder.CreateTable(
                name: "AdjuntoHistorico",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Archivo = table.Column<byte[]>(nullable: true),
                    ContentType = table.Column<string>(nullable: true),
                    Nombre = table.Column<string>(nullable: true),
                    PacienteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdjuntoHistorico", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdjuntoHistorico_Paciente_PacienteId",
                        column: x => x.PacienteId,
                        principalTable: "Paciente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdjuntoHistorico_PacienteId",
                table: "AdjuntoHistorico",
                column: "PacienteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdjuntoHistorico");

            migrationBuilder.AddColumn<byte[]>(
                name: "Adjunto",
                table: "Atencion",
                nullable: true);
        }
    }
}
