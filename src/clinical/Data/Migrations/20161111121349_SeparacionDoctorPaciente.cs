﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace clinical.Data.Migrations
{
    public partial class SeparacionDoctorPaciente : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Direccion",
                table: "Doctor",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EstadoCivil",
                table: "Doctor",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "FecNacimiento",
                table: "Doctor",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Sexo",
                table: "Doctor",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DoctorId",
                table: "Paciente",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Paciente_DoctorId",
                table: "Paciente",
                column: "DoctorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Paciente_Doctor_DoctorId",
                table: "Paciente",
                column: "DoctorId",
                principalTable: "Doctor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Paciente_Doctor_DoctorId",
                table: "Paciente");

            migrationBuilder.DropIndex(
                name: "IX_Paciente_DoctorId",
                table: "Paciente");

            migrationBuilder.DropColumn(
                name: "Direccion",
                table: "Doctor");

            migrationBuilder.DropColumn(
                name: "EstadoCivil",
                table: "Doctor");

            migrationBuilder.DropColumn(
                name: "FecNacimiento",
                table: "Doctor");

            migrationBuilder.DropColumn(
                name: "Sexo",
                table: "Doctor");

            migrationBuilder.AlterColumn<int>(
                name: "DoctorId",
                table: "Paciente",
                nullable: false);
        }
    }
}
