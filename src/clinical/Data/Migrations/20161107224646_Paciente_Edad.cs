﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace clinical.Data.Migrations
{
    public partial class Paciente_Edad : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Edad",
                table: "Paciente");

            migrationBuilder.AlterColumn<DateTime>(
                name: "FecNacimiento",
                table: "Paciente",
                nullable: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Edad",
                table: "Paciente",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AlterColumn<string>(
                name: "FecNacimiento",
                table: "Paciente",
                nullable: true);
        }
    }
}
