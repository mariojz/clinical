﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace clinical.Data.Migrations
{
    public partial class Doctor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Atencion_Paciente_PacienteId",
                table: "Atencion");

            migrationBuilder.DropIndex(
                name: "IX_Atencion_PacienteId",
                table: "Atencion");

            migrationBuilder.DropColumn(
                name: "Doctor",
                table: "Paciente");

            migrationBuilder.DropColumn(
                name: "Doctor",
                table: "Atencion");

            migrationBuilder.DropColumn(
                name: "PacienteId",
                table: "Atencion");

            migrationBuilder.CreateTable(
                name: "Doctor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApMaterno = table.Column<string>(nullable: true),
                    ApPaterno = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Especialidad = table.Column<string>(nullable: true),
                    Nombre = table.Column<string>(nullable: true),
                    Rut = table.Column<string>(nullable: true),
                    Telefono = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Doctor", x => x.Id);
                });

            migrationBuilder.AddColumn<int>(
                name: "DoctorRefId",
                table: "Paciente",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Paciente",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Telefono",
                table: "Paciente",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DoctorRefId",
                table: "Atencion",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PacienteRefId",
                table: "Atencion",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Paciente_DoctorRefId",
                table: "Paciente",
                column: "DoctorRefId");

            migrationBuilder.CreateIndex(
                name: "IX_Atencion_DoctorRefId",
                table: "Atencion",
                column: "DoctorRefId");

            migrationBuilder.CreateIndex(
                name: "IX_Atencion_PacienteRefId",
                table: "Atencion",
                column: "PacienteRefId");

            migrationBuilder.AddForeignKey(
                name: "FK_Atencion_Doctor_DoctorRefId",
                table: "Atencion",
                column: "DoctorRefId",
                principalTable: "Doctor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Atencion_Paciente_PacienteRefId",
                table: "Atencion",
                column: "PacienteRefId",
                principalTable: "Paciente",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Paciente_Doctor_DoctorRefId",
                table: "Paciente",
                column: "DoctorRefId",
                principalTable: "Doctor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Atencion_Doctor_DoctorRefId",
                table: "Atencion");

            migrationBuilder.DropForeignKey(
                name: "FK_Atencion_Paciente_PacienteRefId",
                table: "Atencion");

            migrationBuilder.DropForeignKey(
                name: "FK_Paciente_Doctor_DoctorRefId",
                table: "Paciente");

            migrationBuilder.DropIndex(
                name: "IX_Paciente_DoctorRefId",
                table: "Paciente");

            migrationBuilder.DropIndex(
                name: "IX_Atencion_DoctorRefId",
                table: "Atencion");

            migrationBuilder.DropIndex(
                name: "IX_Atencion_PacienteRefId",
                table: "Atencion");

            migrationBuilder.DropColumn(
                name: "DoctorRefId",
                table: "Paciente");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Paciente");

            migrationBuilder.DropColumn(
                name: "Telefono",
                table: "Paciente");

            migrationBuilder.DropColumn(
                name: "DoctorRefId",
                table: "Atencion");

            migrationBuilder.DropColumn(
                name: "PacienteRefId",
                table: "Atencion");

            migrationBuilder.DropTable(
                name: "Doctor");

            migrationBuilder.AddColumn<string>(
                name: "Doctor",
                table: "Paciente",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Doctor",
                table: "Atencion",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PacienteId",
                table: "Atencion",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Atencion_PacienteId",
                table: "Atencion",
                column: "PacienteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Atencion_Paciente_PacienteId",
                table: "Atencion",
                column: "PacienteId",
                principalTable: "Paciente",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
