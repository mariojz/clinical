﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace clinical.Data.Migrations
{
    public partial class Adjunto : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Paciente_Doctor_DoctorRefId",
                table: "Paciente");

            migrationBuilder.DropIndex(
                name: "IX_Paciente_DoctorRefId",
                table: "Paciente");

            migrationBuilder.DropColumn(
                name: "DoctorRefId",
                table: "Paciente");

            migrationBuilder.DropTable(
                name: "Atencion");

            migrationBuilder.AddColumn<int>(
                name: "DoctorId",
                table: "Paciente",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DoctorId",
                table: "Paciente");

            migrationBuilder.CreateTable(
                name: "Atencion",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Comentario = table.Column<string>(nullable: true),
                    DoctorRefId = table.Column<int>(nullable: true),
                    Fecha = table.Column<DateTime>(nullable: false),
                    PacienteRefId = table.Column<int>(nullable: true),
                    Tipo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Atencion", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Atencion_Doctor_DoctorRefId",
                        column: x => x.DoctorRefId,
                        principalTable: "Doctor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Atencion_Paciente_PacienteRefId",
                        column: x => x.PacienteRefId,
                        principalTable: "Paciente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.AddColumn<int>(
                name: "DoctorRefId",
                table: "Paciente",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Paciente_DoctorRefId",
                table: "Paciente",
                column: "DoctorRefId");

            migrationBuilder.CreateIndex(
                name: "IX_Atencion_DoctorRefId",
                table: "Atencion",
                column: "DoctorRefId");

            migrationBuilder.CreateIndex(
                name: "IX_Atencion_PacienteRefId",
                table: "Atencion",
                column: "PacienteRefId");

            migrationBuilder.AddForeignKey(
                name: "FK_Paciente_Doctor_DoctorRefId",
                table: "Paciente",
                column: "DoctorRefId",
                principalTable: "Doctor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
