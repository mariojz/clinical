﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace clinical.Data.Migrations
{
    public partial class NameEmailID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Apellido",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IdEmpresa",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Nombre",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Atencion_DoctorId",
                table: "Atencion",
                column: "DoctorId");

            migrationBuilder.CreateIndex(
                name: "IX_Atencion_PacienteId",
                table: "Atencion",
                column: "PacienteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Atencion_Doctor_DoctorId",
                table: "Atencion",
                column: "DoctorId",
                principalTable: "Doctor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Atencion_Paciente_PacienteId",
                table: "Atencion",
                column: "PacienteId",
                principalTable: "Paciente",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Atencion_Doctor_DoctorId",
                table: "Atencion");

            migrationBuilder.DropForeignKey(
                name: "FK_Atencion_Paciente_PacienteId",
                table: "Atencion");

            migrationBuilder.DropIndex(
                name: "IX_Atencion_DoctorId",
                table: "Atencion");

            migrationBuilder.DropIndex(
                name: "IX_Atencion_PacienteId",
                table: "Atencion");

            migrationBuilder.DropColumn(
                name: "Apellido",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "IdEmpresa",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Nombre",
                table: "AspNetUsers");
        }
    }
}
