﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace clinical.Data.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Paciente",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApMaterno = table.Column<string>(nullable: true),
                    ApPaterno = table.Column<string>(nullable: true),
                    Direccion = table.Column<string>(nullable: true),
                    Doctor = table.Column<string>(nullable: true),
                    Edad = table.Column<decimal>(nullable: false),
                    EstadoCivil = table.Column<string>(nullable: true),
                    FecNacimiento = table.Column<string>(nullable: true),
                    IdEmpresa = table.Column<string>(nullable: true),
                    Nombre = table.Column<string>(nullable: true),
                    Prevision = table.Column<string>(nullable: true),
                    Rut = table.Column<string>(nullable: true),
                    Sexo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Paciente", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Atencion",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Comentario = table.Column<string>(nullable: true),
                    Doctor = table.Column<string>(nullable: true),
                    Fecha = table.Column<DateTime>(nullable: false),
                    PacienteId = table.Column<int>(nullable: true),
                    Tipo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Atencion", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Atencion_Paciente_PacienteId",
                        column: x => x.PacienteId,
                        principalTable: "Paciente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Atencion_PacienteId",
                table: "Atencion",
                column: "PacienteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Atencion");

            migrationBuilder.DropTable(
                name: "Paciente");
        }
    }
}
