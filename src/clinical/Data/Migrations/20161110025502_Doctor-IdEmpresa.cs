﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace clinical.Data.Migrations
{
    public partial class DoctorIdEmpresa : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "IdEmpresa",
                table: "Doctor",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Rut",
                table: "Paciente",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "Nombre",
                table: "Paciente",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "ApPaterno",
                table: "Paciente",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "Rut",
                table: "Doctor",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "Nombre",
                table: "Doctor",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "ApPaterno",
                table: "Doctor",
                nullable: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdEmpresa",
                table: "Doctor");

            migrationBuilder.AlterColumn<string>(
                name: "Rut",
                table: "Paciente",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Nombre",
                table: "Paciente",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ApPaterno",
                table: "Paciente",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Rut",
                table: "Doctor",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Nombre",
                table: "Doctor",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ApPaterno",
                table: "Doctor",
                nullable: true);
        }
    }
}
