﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using clinical.Models;

namespace clinical.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public DbSet<Paciente> Paciente { get; set; }

        public DbSet<Doctor> Doctor { get; set; }

        public DbSet<Prevision> Prevision { get; set; }

        public DbSet<Especialidad> Especialidad { get; set; }

        public DbSet<Atencion> Atencion { get; set; }

        public DbSet<Adjunto> Adjunto { get; set; }

        public DbSet<AdjuntoHistorico> AdjuntoHistorico { get; set; }
    }
}
